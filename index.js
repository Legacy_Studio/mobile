/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from "./App";
import _App from "./app/index";
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => _App);
