import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import Routes from "./Routes";
// === Importing Pages
import Timeline from "../View/Timeline";
import Settings from "../View/Settings";
import MyCards from "../View/MyCards";
import Search from "../View/Search";

const _Routes = {
    Timeline: Timeline,
    MyCards: MyCards,
    Search: Search,
    Settings: Settings,
}

const Config = {
    initialRouteName: "Timeline",
    defaultNavigationOptions:{
        inactiveBackgroundColor: '#2196F3',
        activeBackgroundColor: "#fff",
        inactiveTintColor: '#fff',
        activeTintColor: "#2196F3"
    }
}

export default createBottomTabNavigator(_Routes, Config);