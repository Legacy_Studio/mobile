import { createStackNavigator, createAppContainer } from "react-navigation";
import Routes from "./Routes";
// === Importing Pages
import Login from "../View/Login";
import TabNavigator from "./TabNavigator";

const _Routes = {
    Login: Login,
    Home: TabNavigator
}

const Config = {
    initialRouteName: "Login",
    defaultNavigationOptions :{
        header : null,
    }
}

export default createStackNavigator(_Routes, Config);