import { createStore, applyMiddleware } from "redux";
import logger  from "redux-logger";
import thunk from "redux-thunk"

// === Importing reducers
import Reducers from "../Service/Reducers";
const middleware = applyMiddleware(thunk, logger);
export default createStore(Reducers, middleware);