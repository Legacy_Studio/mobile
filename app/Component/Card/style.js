import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { width, height } = Dimensions.get('window');

const CardWidth = width * 0.8;
const CardHeight = 150;

export default StyleSheet.create({
    CardWrapper:{
        paddingVertical:15,
        width: CardWidth,
    },
    DateWrapper:{
        backgroundColor: "#fff",
        alignSelf: "flex-start",
        width: width* 0.4,
        marginLeft:25,
        marginBottom: 15,
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 6,
        borderRadius:15,
        borderWidth:1,
        borderColor: "#2a2a2a",
    },
    DateText:{

    },
    Card:{
        position: 'relative',
        width: CardWidth,
        height: CardHeight,
        borderWidth: 1,
        borderColor: "#2a2a2a",
        backgroundColor: "#fff",
    },
    CardInfoWrapper:{
        position: "absolute",
        bottom: 0,
        left: 0,
        width: CardWidth - 2,
        paddingLeft: 10,
        paddingBottom: 10,
    },
    CardName:{
        fontSize:16,
    },
    CardTitleWrapper:{
        flexDirection: 'row'
    },
    CardTitle:{
        fontSize:12,
        fontStyle: 'italic'
    },
    CardCompany:{
        marginLeft:6,
        fontSize:12,
        fontStyle: 'italic',
        fontWeight: "bold"
    },
    CardEmail:{
        fontSize:12,
        textDecorationStyle: "solid"
    },
    CardPhoneNumber:{
        fontSize:12,
    },
    CardLogo:{
        position:'absolute',
    },
    HoverMenu:{
        position: "absolute",
        top:0,
        left:0,
        width: CardWidth -2,
        height: CardHeight -2,
        backgroundColor: "#2a2a2aDD",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        zIndex:11,
    },
    HoverMenuItem:{
        borderWidth:1,
        borderColor: "#fff",
        width:60,
        height:60,
        borderRadius:30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    HoverMenuClose:{
        position:'absolute',
        top:5,
        right:5,
        width:30,
        height:30,
        borderRadius:15,
        backgroundColor:"#fff",
        justifyContent:'center',
        alignItems:'center'
    }
});