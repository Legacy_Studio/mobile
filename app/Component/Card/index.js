import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions
} from "react-native";
// === Importing style
import styles from "./style";
import Ionicons from "react-native-vector-icons/Ionicons";

const { width, height } = Dimensions.get('window');

const CardWidth = width * 0.8;

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            isHoverActive: false,
        }
    }
    
    renderDate(){
        if(this.props.dateVisible){
            return(
                <View style={ styles.DateWrapper }>
                    <Text>{ this.props.data.date }</Text>
                </View>
            )
        }
    }

    enableHoverMenu(){
        this.setState({ isHoverActive: true})
    }
    
    disableHoverMenu(){
        this.setState({ isHoverActive: false})
    }

    renderHoverMenu(){
        if(this.state.isHoverActive){
            return(
                <View style={ styles.HoverMenu}>
                    <TouchableOpacity
                        onPress={() => { alert("Add to contact")}}
                        style={styles.HoverMenuItem}
                        >
                        <View>
                            <Ionicons 
                                name={"ios-person-add"}
                                size={40}
                                color={"#fff"}
                            />
                        </View>
                    </TouchableOpacity>     
                    <TouchableOpacity
                        onPress={() => { alert("Add to contact")}}
                        style={styles.HoverMenuItem}
                        >
                        <View>
                            <Ionicons 
                                name={"ios-call"}
                                size={40}
                                color={"#fff"}
                            />
                        </View>
                    </TouchableOpacity>     
                    <TouchableOpacity
                        onPress={() => { alert("Add to contact")}}
                        style={styles.HoverMenuItem}
                        >
                        <View>
                            <Ionicons 
                                name={"ios-mail"}
                                size={40}
                                color={"#fff"}
                            />
                        </View>
                    </TouchableOpacity>     
                    <TouchableOpacity
                        onPress={()=>{ this.disableHoverMenu() }}
                        style={ styles.HoverMenuClose }
                        >
                        <View>
                            <Ionicons 
                                name="ios-close"
                                size={40}
                                color={"#2a2a2a"}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            )
        } else {
            return null
        }
    }

    render(){
        return(
            <View style={[ 
                    styles.CardWrapper,
                    {
                        width: this.props.center ? width : CardWidth,
                        alignItems: this.props.center ? "center" : "flex-start" 
                    },
                    {
                        borderBottomWidth: this.props.borderBottom ? 1 : 0,
                        borderColor:"#ddd"
                    }
                ]}>
                { this.renderDate() }
                <View 
                    style={ styles.Card }>
                    <TouchableOpacity
                        onPress={()=>{ this.enableHoverMenu() }}
                        style={{flex:1}}
                        >
                        <View style={ styles.CardInfoWrapper}>
                            <View>
                                <Text style={ styles.CardName }>{this.props.data.name}</Text>
                            </View>
                            <View style={ styles.CardTitleWrapper }>
                                <Text style={ styles.CardTitle }>{ this.props.data.title }</Text>
                                <Text style={ styles.CardCompany }>{ this.props.data.company ? `@ ${this.props.data.company }` : ''}</Text>
                            </View>
                            <View>
                                <Text style={ styles.CardEmail }>{ this.props.data.email }</Text>
                            </View>
                            <View>
                                <Text style={ styles.CardPhoneNumber }>{ this.props.data.phoneNumber}</Text>
                            </View>
                        </View>
                        {
                            this.props.data.images ?
                                this.props.data.images.map((item, index) => {
                                    return(
                                        <View key={index}
                                            style={[
                                                styles.CardLogo,
                                                {
                                                    top: item.top,
                                                    left:item.left,
                                                    right: item.right,
                                                    bottom: item.bottom
                                                }
                                            ]}
                                            >
                                            <Image
                                                style={{width: 50, height: 50}}
                                                source={ require(`../../Resources/Images/3.png`) }
                                            />
                                        </View>
                                    )
                                })
                                :
                                null
                        }
                    </TouchableOpacity>
                    {
                        this.renderHoverMenu()
                    }
                </View>
            </View>
        );
    }
}

export default Index;