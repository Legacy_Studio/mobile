import React, { Component } from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity
} from "react-native";
import styles from "./style";

import Card from "../../Component/Card";

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            myCards: MyCards,
            activeCard: 0
        }
    }

    setActiveCard(index){
        if(this.state.activeCard && this.state.activeCard == index){
            this.setState({
                activeCard: null
            })
        } else {
            this.setState({
                activeCard: index
            })
        }
    }

    isActivatorActivated(index){
        if(this.state.activeCard == index){
            return {
                backgroundColor: "#33FF99",
                borderColor: "#00CC66"
            }
        } 
    }

    renderMyCards(){
        if(this.state.myCards && this.state.myCards.length > 0){
            return this.state.myCards.map((item, index ) => {
                return(
                    <View key={index} 
                        style={[ 
                            styles.CardWrapper,
                            {
                                backgroundColor: this.state.activeCard == index ? "#33FF9933" : "#fff"
                            }
                        ]}>
                        <TouchableOpacity
                            onPress={()=>{ this.setActiveCard(index) }}
                            style={[
                                styles.ActivatorBtn,
                                this.isActivatorActivated(index)
                            ]}
                        />
                        <Card 
                            data={item}
                            key={index}
                        />
                    </View>
                );й
            })
        } else {
            return(
                <View>
                    <Text>You don't have a business card yet.</Text>
                </View>
            )
        }
    }

    render(){
        return(
            <View style={ styles.PageWrapper }>
                <ScrollView style={ styles.ScrollViewStyle }>
                    {
                        this.renderMyCards()
                    }
                </ScrollView>
            </View>
        );
    }
}

export default Index;

const MyCards = [
    {
        name: 'Nomio',
        title: "CEO & CTO",
        company: "My Card",
        email: "nomikeeper@gmail.com",
        phoneNumber: "080-1982-5003",
        date: '2019-02-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Nomio',
        title: "Singer & Songwriter",
        company: "",
        email: "nomikeeper@gmail.com",
        phoneNumber: "080-1982-5003",
        date: '2019-02-25',
        images:[
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Nomio',
        title: "Software Engineer",
        company: "BitCraft",
        email: "nomikeeper@gmail.com",
        phoneNumber: "080-1982-5003",
        date: '2019-02-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
        ]
    }
]