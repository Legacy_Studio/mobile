import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
    PageWrapper:{
        flex:1,
    },
    ScrollViewStyle:{
        flex:1,
    },
    CardWrapper:{
        borderBottomWidth:1,
        borderColor: "#ddd",
        paddingRight:10,
        justifyContent:"flex-end",
        flexDirection:'row',
    },
    ActivatorBtn:{
        borderRightWidth:1,
        borderColor:"#ddd",
        width:width*0.2 - 19,
        marginRight:10,
        backgroundColor:"#eaeaea",
    }
})