import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { } = Dimensions.get('window');

export default StyleSheet.create({
    PageWrapper:{
        flex:1
    },
    ScrollView:{
        flex:1,
    },
    OptionHeader:{
        paddingHorizontal:15,
        paddingVertical:10,
        borderTopWidth:1,
        borderBottomWidth:1,
        backgroundColor:"#eaeaea",
        borderColor:"#666"
    },
    OptionHeaderText:{
        fontSize:20,
        fontWeight:"bold"
    },
    OptionRow:{
        flexDirection:"row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor:"#fff",
        paddingVertical:10,
        paddingHorizontal: 15,
        borderBottomWidth:1,
        borderColor:"#ddd"
    },
    OptionImageRow:{
        justifyContent:"center",
        alignItems:"center",
        marginTop:15,
    },
    ImageWrapper:{
        width:150,
        height:150,
        borderWidth:1,
        borderColor:"#eaeaea",
    },
    Image:{
    },
    UploadImageButton:{
        width:148,
        height:148,
        justifyContent:"center",
        alignItems:"center",
    },
    AccountsWrapper:{

    },
    AccountCheckmarkWrapper:{
        width:40,
        height:40,
        borderRadius:20,
        backgroundColor:"#33CC99",
        justifyContent:"center",
        alignItems:"center"
    },
    OptionButtonRow:{
        flexDirection:"row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor:"#fff",
        borderBottomWidth:1,
        borderColor:"#ddd",
    },
    OptionButton:{
        flex:1,
        paddingVertical:10,
        paddingHorizontal: 15,
        justifyContent: "center",
        alignItems: "center",
    },
    OptionButtonText:{
        fontSize:20,
        fontWeight: "bold",
    }
});