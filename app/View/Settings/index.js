import React, { Component } from "react";
import {
    View,
    Text,
    Switch,
    TouchableOpacity,
    Image,
    ScrollView
} from "react-native";

import styles from "./style";

import Ionicons from "react-native-vector-icons/Ionicons";

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            imageViewable: false,
            makeMeVisible: false,
            accounts: Accounts,
            image: ''
        }
    }

    changeMakeMeVisible(value){
        this.setState({ makeMeVisible: value })
    }

    changeImageViewable(value){
        this.setState({ imageViewable: value })
    }

    renderAccounts(){
            return this.state.accounts.map((item,index) => {
                console.log(item.name + " IsLinked ? " + item.isLinked)
                return(
                    <View key={ index }
                        style={ styles.OptionRow }>
                        <Text>{ item.name }</Text>
                        {
                            item.isLinked == true ?
                                <View style={ styles.AccountCheckmarkWrapper }>
                                    <Ionicons 
                                        name="ios-checkmark"
                                        size={40}
                                        color={"#fff"}
                                    />
                                </View>
                                :
                                <TouchableOpacity
                                    onPress={()=>{ alert("Connect!")}}
                                    >
                                    <View>
                                        <Text>Connect</Text>
                                    </View>
                                </TouchableOpacity>
                        }
                    </View>
                )
            })
    }

    renderProfileImage(){
        if(this.state.image){
            return(
                <Image 
                    style={styles.Image}
                    source={{ uri: "https://www.myagecalculator.com/images/brad-pitt.jpg"}}
                />
            );
        } else {
            return(
                <TouchableOpacity
                    style={ styles.UploadImageButton }
                    onPress={()=>{ alert("Upload Image")}}
                    >
                    <View>
                        <Ionicons 
                            name={"ios-image"}
                            size={60}
                            color={"#eaeaea"}
                        />
                    </View>
                </TouchableOpacity>
            );
        }
    }

    logout(){
        this.props.navigation.replace("Login");
    }

    render(){
        return(
            <View style={ styles.PageWrapper }>
                <ScrollView style={ styles.ScrollView }>
                    <View style={ styles.OptionHeader }>
                        <Text style={ styles.OptionHeaderText }>Public image</Text>
                    </View>
                    <View style={ styles.OptionImageRow }>
                        <View style={ styles.ImageWrapper }>
                            { this.renderProfileImage() }
                        </View>
                    </View>
                    <View style={ styles.OptionRow }>
                        <Text>Viewable</Text>
                        <Switch 
                            value={this.state.imageViewable}
                            onValueChange={( value )=>{ this.changeImageViewable( value ) }}
                        />
                    </View>
                    <View style={ styles.OptionHeader }>
                        <Text style={ styles.OptionHeaderText }>Settings</Text>
                    </View>
                    <View style={ styles.OptionRow }>
                        <Text>Make me visible</Text>
                        <Switch 
                            value={this.state.makeMeVisible}
                            onValueChange={( value )=>{ this.changeMakeMeVisible( value ) }}
                        />
                    </View>
                    <View style={ styles.OptionHeader }>
                        <Text style={ styles.OptionHeaderText }>Linked accounts</Text>
                    </View>
                    <View styles={ styles.AccountsWrapper }>
                        {
                            this.renderAccounts()
                        }
                    </View>
                    <View style={ styles.OptionHeader }>
                        <Text style={ styles.OptionHeaderText }>Other services</Text>
                    </View>
                    <View style={ styles.OptionButtonRow }>
                        <TouchableOpacity
                            onPress={()=>{ alert("Print") }}
                            style={ styles.OptionButton }
                            >
                            <View>
                                <Text style={ styles.OptionButtonText }>Print out</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={ styles.OptionButtonRow }>
                        <TouchableOpacity
                            onPress={()=>{ alert("Export") }}
                            style={ styles.OptionButton }
                            >
                            <View>
                                <Text style={ styles.OptionButtonText }>Export as</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={ styles.OptionButtonRow }>
                        <TouchableOpacity
                            onPress={()=>{ this.logout() }}
                            style={ styles.OptionButton }
                            >
                            <View>
                                <Text style={ styles.OptionButtonText }>Log out</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default Index;

const Accounts = [
    {
        name: "Morphius",
        isLinked: false
    },
    {
        name: "Linked in",
        isLinked: false
    },
    {
        name: "Facebook",
        isLinked: false
    },
    {
        name: "Google plus",
        isLinked: false
    },
]