import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    PageWrapper:{
        flex:1,
        alignItems: 'center',
    },
    Form:{
        marginTop:120,
        width: width* 0.8,
        minHeight:200,
        paddingVertical:25,
    },
    LoginBtnWrapper:{
        marginTop:25,
        justifyContent:"center",
        alignItems:"center"
    },
    LoginBtn:{
        backgroundColor: "#2196F3",
        paddingHorizontal: 50,
        paddingVertical: 10,
        borderRadius:25,
    },
    InputWrapper:{
        paddingHorizontal:25,
        paddingVertical:10,
    },
    LoginBtnText:{
        color:"#fff"
    },
    TextInputWrapper:{
    },
    TextInput:{
        borderWidth: 1,
        borderColor:"#2a2a2a",
        borderRadius:25,
        paddingHorizontal:15,
        paddingVertical:7,
        height:40,
        backgroundColor:"#fff",
    },
    TextInputClear:{
        position:'absolute',
        backgroundColor: '#eaeaea',
        borderWidth:1,
        borderColor:"#aaa",
        borderRadius:20,
        right: 3,
        top:2,
        width:36,
        height:36,
        justifyContent:'center',
        alignItems:'center',
    },
})