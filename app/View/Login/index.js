import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
} from "react-native";
// === Importing styles
import styles from "./style";

import Ionicons from "react-native-vector-icons/Ionicons";

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            credentials:{
                username:'',
                password:''
            }
        }
    }

    // Login function
    login(){
        this.props.navigation.replace("Home");
    }

    updateValue(type, value){
        switch(type){
            case InputType.username:{
                let newCredentials = this.state.credentials;
                newCredentials.username = value
                this.setState({
                    credentials: newCredentials
                })
                break;
            }
            case InputType.password:{
                let newCredentials = this.state.credentials;
                newCredentials.password = value
                this.setState({
                    credentials: newCredentials
                })
                break;
            }
            default: return;
        }
    }
    render(){
        return(
            <View style={ styles.PageWrapper }>
                <View style={ styles.Form }>
                    {/* Username Field */}
                    <View style={styles.InputWrapper}>
                        <View style={ styles.TextInputWrapper }>
                            <TextInput 
                                placeholder={"Username"}
                                style={ styles.TextInput }
                                value={this.state.credentials.username}
                                onChangeText={(value)=>{ this.updateValue(InputType.username, value) }}
                            />
                            {
                                this.state.credentials.username != '' ?
                                <TouchableOpacity 
                                    style={ styles.TextInputClear }
                                    onPress={()=>{ this.updateValue(InputType.name, "")}}
                                    >
                                    <View>
                                        <Ionicons 
                                            name={'ios-close'}
                                            size={30}
                                            color={"#aaa"}
                                        />
                                    </View>
                                </TouchableOpacity>
                                :
                                null
                            }
                        </View>
                    </View>
                    
                    {/* Password Field */}
                    <View style={styles.InputWrapper}>
                        <View style={ styles.TextInputWrapper }>
                            <TextInput 
                                placeholder={"Password"}
                                secureTextEntry={true}
                                style={ styles.TextInput }
                                value={this.state.credentials.password}
                                onChangeText={(value)=>{ this.updateValue(InputType.password, value) }}
                            />
                            {
                                this.state.credentials.password != '' ?
                                <TouchableOpacity 
                                    style={ styles.TextInputClear }
                                    onPress={()=>{ this.updateValue(InputType.password, "")}}
                                    >
                                    <View>
                                        <Ionicons 
                                            name={'ios-close'}
                                            size={30}
                                            color={"#aaa"}
                                        />
                                    </View>
                                </TouchableOpacity>
                                :
                                null
                            }
                        </View>
                    </View>
                    <View style={ styles.LoginBtnWrapper }>
                        <TouchableOpacity
                            style={styles.LoginBtn}
                            onPress={() => { this.login() }}
                            >
                            <View>
                                <Text style={ styles.LoginBtnText }>Login</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </View>
        );
    }
}

export default Index;

const InputType={
    username: 'username',
    password: 'password'
}