import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    PageWrapper:{
        flex:1
    },
    ScrollView:{
        flex:1,
    },
    RowWrapper:{
        borderBottomWidth:1,
        borderColor:"#ddd",
    },
    RowContentWrapper:{
        flexDirection:'row',
        alignItems:"center",
        justifyContent: "space-between",
    },
    PersonButton:{
        paddingHorizontal:20,
        paddingVertical:10,
    },
    PersonInfoWrapper:{

    },
    PersonName:{
        fontSize:20,
        fontWeight: "bold"
    },
    PersonTitle:{
        fontSize:14,
        fontStyle: "italic"
    },
    PersonCompany:{
        fontWeight:"bold",
        fontSize:14,
        fontStyle: "italic"
    },
    NoneWrapper:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
    },
    NoneMessageWrapper:{
        width:150,
    },
    NoneMessageText:{
        fontSize:18,
        lineHeight:28,
        textAlign: "center",
    }
})