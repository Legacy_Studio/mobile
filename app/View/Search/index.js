import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
} from "react-native";
// Importing styles
import styles from "./style";

import Ionicons from "react-native-vector-icons/Ionicons"

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            nearbyPeople: NearbyPeople
        }
    }    

    // Render a person row
    renderPerson(data, index){
        return(
            <View key={index} 
                style={ styles.RowWrapper }>
                <TouchableOpacity
                    style={ styles.PersonButton }
                    onPress={()=>{ alert("View Profile Image.") }}
                    >
                    <View style={ styles.RowContentWrapper }>
                        <View style={ styles.PersonInfoWrapper }>
                            <Text style={ styles.PersonName }>{ data.name }</Text>
                            <Text style={ styles.PersonTitle }>
                                {`${data.title} @ `}
                                <Text style={ styles.PersonCompany }>
                                    {data.company}
                                </Text>
                            </Text>
                        </View>
                        <View>
                            <Ionicons
                                name={"ios-image"}
                                size={30}
                                color={"#33CC99"}
                            />
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    renderPeople(){
        if(this.state.nearbyPeople && this.state.nearbyPeople.length > 0){
            return this.state.nearbyPeople.map((item, index) => {
                return this.renderPerson(item,index)
            })
        } else {
            return(
                <View style={ styles.NoneWrapper }>
                    <Ionicons 
                        name={"md-globe"}
                        size={40}
                        color={"#2a2a2a"}
                    />
                    <View style={ styles.NoneMessageWrapper }>
                        <Text style={ styles.NoneMessageText }>No one nearby at the moment</Text>
                    </View>
                </View>
            )
        }
    }

    render(){
        return(
            <View style={ styles.PageWrapper}>
                <ScrollView style={ styles.ScrollView }>
                    { this.renderPeople() }
                </ScrollView>
            </View> 
        )
    }
}

export default Index;

const NearbyPeople = [
    {
        name: 'Nomio',
        title: "CEO & CTO",
        company: "My Card",
        email: "nomikeeper@gmail.com",
        phoneNumber: "080-1982-5003",
        date: '2019-02-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Mendbayar',
        title: "CFO",
        company: "My Card",
        email: "mendbayar_official@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-01-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
        ]
    },
    {
        name: 'David Guesta',
        title: "Lead Software Engineer",
        company: "My Card",
        email: "DaveGuesta@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-05',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Edward Shool',
        title: "Marketing Manager",
        company: "Kojima Production",
        email: "edward0412@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-05',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Edward Stool',
        title: "Manager",
        company: "Delta Angle",
        email: "edwardelta@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-06',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    }
]