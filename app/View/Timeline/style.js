import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    PageWrapper:{
        flex:1,
        position: "relative"
    },
    absButton:{
        position: "absolute",
        top: 10,
        right: 10,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#fff",
        width:50,
        height:50,
        borderRadius:25,
        borderWidth:1,
        borderColor:"#2a2a2a",
        zIndex:10,
    },
    listWrapper:{
        flex:1,
        backgroundColor: "#eaeaea",
        zIndex:1,
    },
    Modal:{
        flex:1,
        position: 'relative'
    },
    ModalHeader:{
        height:80,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        backgroundColor: "#eaeaea",
        borderBottomWidth:1,
        borderColor: "#2a2a2a",
    },
    ModalHeaderBtn:{
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#fff",
        width:50,
        height:50,
        borderRadius:25,
        borderWidth:1,
        borderColor:"#2a2a2a",
    },
    ModalHeaderBtnText:{
        color: "#2a2a2a",
    },
    ModalScrollWrapper:{
        flex:9,
    },
    ModalScroll:{
    },
    InputWrapper:{
        paddingHorizontal:25,
        paddingVertical:10,
        borderBottomWidth:1,
        borderColor: "#ddd"
    },
    InputLabelWrapper:{

    },
    InputLabel:{
        fontSize:20,
        fontWeight: "bold",
        color: "#2a2a2a",
    },
    TextInputWrapper:{
        marginVertical:10,
    },
    TextInput:{
        borderWidth: 1,
        borderColor:"#2a2a2a",
        borderRadius:25,
        paddingHorizontal:15,
        paddingVertical:7,
        height:40,
        backgroundColor:"#fff",
    },
    TextInputClear:{
        position:'absolute',
        backgroundColor: '#ea3434',
        borderWidth:1,
        borderColor:"#ea3434",
        borderRadius:20,
        right: 3,
        top:2,
        width:36,
        height:36,
        justifyContent:'center',
        alignItems:'center',
    }
})