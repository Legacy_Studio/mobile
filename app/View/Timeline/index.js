import React, { Component } from "react";
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Modal,
    ScrollView,
    TextInput
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Card from "../../Component/Card";

// === Importing styles
import styles from "./style";

const InitialConfig = {
    name: '',
    title: '',
    company: '',
    email: '',
    phoneNumber: ''
}

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            modalState: false,
            cards: TestCards,
            filterConfig: InitialConfig
        }
    }

    // Render card
    renderCard(item, index){
        var dateVisible = true;
        if(index > 0 ){
            dateVisible = this.state.cards[index].date == this.state.cards[index-1].date ? false : true;
        }
        return(
            <Card 
                data={ item }
                key={ index }
                center={ true }
                borderBottom={ true }
                dateVisible={ dateVisible }
            />
        );
    }

    // Filter related functions
    openFilter(){
        this.setState({ modalState: true });
    }

    closeFilter(command){
        if(command == "close"){
            this.updateFilterValue("ClearAll", '');
        }
        this.setState({ modalState: false });
    }

    renderSetButton(){
            return(
                <TouchableOpacity
                    onPress={()=>{ this.closeFilter() }}
                    style={[ 
                        styles.ModalHeaderBtn,
                        {
                            backgroundColor: "#33FF99",
                            borderColor: "#00CC66"
                        } 
                    ]}
                    >
                    <View>
                        <Ionicons 
                            name="ios-checkmark"
                            size={50}
                            color={"#fff"}
                        />
                    </View>
                </TouchableOpacity>
            )
    }

    // Update Filter config
    updateFilterValue(type, value){
        let newConfig = this.state.filterConfig;
        switch(type){
            case InputType.name:{
                newConfig.name = value
                this.setState({
                    filterConfig: newConfig
                });
                return;
            }
            case InputType.title:{
                newConfig.title = value
                this.setState({
                    filterConfig: newConfig
                });
                return;
            }
            case InputType.company:{
                newConfig.company = value
                this.setState({
                    filterConfig: newConfig
                });
                return;
            }
            case InputType.email:{
                newConfig.email = value
                this.setState({
                    filterConfig: newConfig
                });
                return;
            }
            case InputType.phoneNumber:{
                newConfig.phoneNumber = value
                this.setState({
                    filterConfig: newConfig
                });
                return;
            }
            case "ClearAll":{
                this.setState({
                    filterConfig: InitialConfig
                })
                return;
            }
            default: return;
        }
    }

    render(){
        return(
            <View style={styles.PageWrapper}>
                <View style={styles.listWrapper}>
                    <FlatList 
                        data={ this.state.cards }
                        renderItem={ ({item, index}) => this.renderCard(item, index)}
                    />
                </View>
                
                <TouchableOpacity
                    onPress={()=>{ this.openFilter() }}
                    style={ styles.absButton }
                    >
                    <View>
                        <Ionicons 
                            name="ios-settings"
                            size={30}
                            color={"#2a2a2a"}
                        />
                    </View>
                </TouchableOpacity>

                <Modal
                    animated={true}
                    animationType="slide"
                    visible={this.state.modalState}
                    style={ styles.Modal }
                    >
                    <View style={styles.ModalHeader}>
                        <TouchableOpacity
                            onPress={()=>{ this.closeFilter("close") }}
                            style={ styles.ModalHeaderBtn }
                            >
                            <View>
                                <Ionicons 
                                    name="ios-arrow-back"
                                    size={30}
                                    color={"#2a2a2a"}
                                />
                            </View>
                        </TouchableOpacity>
                        { this.renderSetButton() }
                    </View>
                    <View style={styles.ModalScrollWrapper}>
                        <ScrollView style={ styles.ModalScroll }>
                            {/* Name Field */}
                            <View style={styles.InputWrapper}>
                                <View style={ styles.InputLabelWrapper}>
                                    <Text style={ styles.InputLabel }>Name</Text>
                                </View>
                                <View style={ styles.TextInputWrapper }>
                                    <TextInput 
                                        style={ styles.TextInput }
                                        value={this.state.filterConfig.name}
                                        onChangeText={(value)=>{ this.updateFilterValue(InputType.name, value) }}
                                    />
                                    {
                                        this.state.filterConfig.name != '' ?
                                        <TouchableOpacity 
                                            style={ styles.TextInputClear }
                                            onPress={()=>{ this.updateFilterValue(InputType.name, "")}}
                                            >
                                            <View>
                                                <Ionicons 
                                                    name={'ios-close'}
                                                    size={30}
                                                    color={"#fff"}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                            
                            {/* Title Field */}
                            <View style={styles.InputWrapper}>
                                <View style={ styles.InputLabelWrapper}>
                                    <Text style={ styles.InputLabel }>Title</Text>
                                </View>
                                <View style={ styles.TextInputWrapper }>
                                    <TextInput 
                                        style={ styles.TextInput }
                                        value={this.state.filterConfig.title}
                                        onChangeText={(value)=>{ this.updateFilterValue(InputType.title, value) }}
                                    />
                                    {
                                        this.state.filterConfig.title != '' ?
                                        <TouchableOpacity 
                                            style={ styles.TextInputClear }
                                            onPress={()=>{ this.updateFilterValue(InputType.title, "")}}
                                            >
                                            <View>
                                                <Ionicons 
                                                    name={'ios-close'}
                                                    size={30}
                                                    color={"#fff"}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                                    
                            {/* Company Field */}
                            <View style={styles.InputWrapper}>
                                <View style={ styles.InputLabelWrapper}>
                                    <Text style={ styles.InputLabel }>Company</Text>
                                </View>
                                <View style={ styles.TextInputWrapper }>
                                    <TextInput 
                                        style={ styles.TextInput }
                                        value={this.state.filterConfig.company}
                                        onChangeText={(value)=>{ this.updateFilterValue(InputType.company, value) }}
                                    />
                                    {
                                        this.state.filterConfig.company != '' ?
                                        <TouchableOpacity 
                                            style={ styles.TextInputClear }
                                            onPress={()=>{ this.updateFilterValue(InputType.company, "")}}
                                            >
                                            <View>
                                                <Ionicons 
                                                    name={'ios-close'}
                                                    size={30}
                                                    color={"#fff"}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>

                            {/* email Field */}
                            <View style={styles.InputWrapper}>
                                <View style={ styles.InputLabelWrapper}>
                                    <Text style={ styles.InputLabel }>E-mail</Text>
                                </View>
                                <View style={ styles.TextInputWrapper }>
                                    <TextInput 
                                        style={ styles.TextInput }
                                        value={this.state.filterConfig.email}
                                        onChangeText={(value)=>{ this.updateFilterValue(InputType.email, value) }}
                                    />
                                    {
                                        this.state.filterConfig.email != '' ?
                                        <TouchableOpacity 
                                            style={ styles.TextInputClear }
                                            onPress={()=>{ this.updateFilterValue(InputType.email, "")}}
                                            >
                                            <View>
                                                <Ionicons 
                                                    name={'ios-close'}
                                                    size={30}
                                                    color={"#fff"}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>

                            {/* Phone number Field */}
                            <View style={styles.InputWrapper}>
                                <View style={ styles.InputLabelWrapper}>
                                    <Text style={ styles.InputLabel }>PhoneNumber</Text>
                                </View>
                                <View style={ styles.TextInputWrapper }>
                                    <TextInput 
                                        style={ styles.TextInput }
                                        value={this.state.filterConfig.phoneNumber}
                                        onChangeText={(value)=>{ this.updateFilterValue(InputType.phoneNumber, value) }}
                                    />
                                    {
                                        this.state.filterConfig.phoneNumber != '' ?
                                        <TouchableOpacity 
                                            style={ styles.TextInputClear }
                                            onPress={()=>{ this.updateFilterValue(InputType.phoneNumber, "")}}
                                            >
                                            <View>
                                                <Ionicons 
                                                    name={'ios-close'}
                                                    size={30}
                                                    color={"#fff"}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        );
    }
}

export default Index;

const InputType={
    name: 'name',
    title: 'title',
    company: 'company',
    email: 'email',
    phoneNumber: 'phoneNumber'
}

const TestCards = [
    {
        name: 'Nomio',
        title: "CEO & CTO",
        company: "My Card",
        email: "nomikeeper@gmail.com",
        phoneNumber: "080-1982-5003",
        date: '2019-02-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Mendbayar',
        title: "CFO",
        company: "My Card",
        email: "mendbayar_official@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-01-25',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/2.png',
                top: null,
                left: null,
                right: 10,
                bottom: 10
            },
        ]
    },
    {
        name: 'David Guesta',
        title: "Lead Software Engineer",
        company: "My Card",
        email: "DaveGuesta@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-05',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Edward Shool',
        title: "Marketing Manager",
        company: "Kojima Production",
        email: "edward0412@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-05',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    },
    {
        name: 'Edward Stool',
        title: "Manager",
        company: "Delta Angle",
        email: "edwardelta@gmail.com",
        phoneNumber: "xxx-xxxx-xxxx",
        date: '2019-02-06',
        images:[
            {
                url: '../../Resources/Images/1.png',
                top: 10,
                left: 10,
                right: null,
                bottom: null
            },
            {
                url: '../../Resources/Images/3.png',
                top: 10,
                left: null,
                right: 10,
                bottom: null
            }
        ]
    }
]