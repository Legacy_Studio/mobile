import { combineReducers } from "redux";

// === Importing Reducers
import Timeline from "./Timeline";

export default combineReducers({
    Timeline: Timeline
})