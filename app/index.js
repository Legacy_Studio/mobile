import React, { Component } from "react";
import {
    View,
    Text
} from "react-native";
import { Provider, connect } from "react-redux";
import Store from "./Store";
import Navigator from "./Navigator";

class Index extends Component{
    constructor(props){
        super(props);
        this.state ={

        }
    }

    render(){
        return(
            <View style={{ flex: 1}}>
                <Navigator />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})
// Binding state to Index class props
const _App = connect(mapStateToProps)(Index);

export default App = () => {
    return(
        <Provider store={ Store }>
            <_App />
        </Provider>
    )
}